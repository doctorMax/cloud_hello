FROM nginx:1.20
# COPY ./nginx.conf /etc/nginx/nginx.conf
# COPY ./index.html /etc/nginx/html/index.html
ARG UNAME=testuser
ARG UID=1001
ARG GID=1001

RUN groupadd -g $GID -o $UNAME
RUN useradd -m -u $UID -g $GID -o -s /bin/bash $UNAME

WORKDIR /app

RUN chown -R $UNAME:$UNAME /app && chmod -R 755 /app && \
        chown -R $UNAME:$UNAME /var/cache/nginx && \
        chown -R $UNAME:$UNAME /var/log/nginx && \
        chown -R $UNAME:$UNAME /etc/nginx/conf.d
RUN touch /var/run/nginx.pid && \
        chown -R $UNAME:$UNAME /var/run/nginx.pid
USER $UNAME



COPY ./app /app
# COPY ./files/nginx.default /etc/nginx/sites-avaliable/default
COPY ./files/nginx.default /etc/nginx/conf.d/default.conf
EXPOSE 8000

CMD ["nginx", "-g", "daemon off;"]