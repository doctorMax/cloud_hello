# Makefile для сборки Docker-образа и запуска контейнера Nginx

# Переменные
IMAGE_NAME = my-nginx-server
CONTAINER_NAME = mynginx101

# Цель для сборки Docker-образа
build:
	docker build -t $(IMAGE_NAME) .

# Цель для запуска контейнера из собранного образа
run:
	docker run --rm --name $(CONTAINER_NAME) -d -p 8000:80 $(IMAGE_NAME)

# Цель для остановки и удаления контейнера
stop:
	docker stop $(CONTAINER_NAME) || true
